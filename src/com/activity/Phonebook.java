package com.activity;

import java.util.ArrayList;

public class Phonebook {
    //properties
    private ArrayList<Contact> contacts;

    // constructor
    public Phonebook(){
        this.contacts = new ArrayList<>();
    };

    // setter getter
    public ArrayList<Contact> getPhonebook(){
        return this.contacts;
    }

    public void addContact(Contact person){
        this.contacts.add(person);
        System.out.println(person.getName() + " has been added to the phonebook!");
    }

}
