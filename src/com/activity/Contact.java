package com.activity;

public class Contact {
    //properties
    private String name, contactNumber, address;

    // constructor
    public Contact(String name, String contactNumber, String address){
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }

    // getters
    public String getName(){
      return this.name;
    };

    public String getContactNumber(){
        return this.contactNumber;
    };

    public String getAddress(){
        return this.address;
    };

    // setters
    public void setName(String name){
        this.name = name;
    }

    public void setContactNumber(String contactNumber){
        this.contactNumber = contactNumber;
    }

    public void setAddress(String address){
        this.address = address;
    }

}
