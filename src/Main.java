import com.activity.Contact;
import com.activity.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        Contact person1 = new Contact("Regina Vanguardia", "HI555", "City Proper, Nueva Esperanza");
        Contact person2 = new Contact("Narda Custodio", "DAR911", "Brgy. 143, Nueva Esperanza");

        phonebook.addContact(person1);
        phonebook.addContact(person2);

        if(phonebook.getPhonebook().isEmpty()){
             System.out.println("Phonebook is empty!");
        } else {
            for(Contact person : phonebook.getPhonebook()){
                System.out.println("=============================");
                System.out.println(person.getName());
                System.out.println("-----------------------------");
                System.out.println("Registered Number");
                System.out.println(person.getContactNumber());
                System.out.println("Registered Address:");
                System.out.println(person.getAddress());
                System.out.println();
            }
        }

    }
}